<?php
/**
 * Custom WordPress configurations on "wp-config.php" file.
 *
 * This file has the following configurations: MySQL settings, Table Prefix, Secret Keys, WordPress Language, ABSPATH and more.
 * For more information visit {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php} Codex page.
 * Created using {@link http://generatewp.com/wp-config/ wp-config.php File Generator} on GenerateWP.com.
 *
 * @package WordPress
 * @generator GenerateWP.com
 */


/* MySQL settings */
define( 'DB_NAME',              $_SERVER['DB_NAME'] );
define( 'DB_USER',              $_SERVER['DB_USER'] );
define( 'DB_PASSWORD',          $_SERVER['DB_PASS'] );
define( 'DB_HOST',              $_SERVER['DB_HOST'] );
define( 'DB_CHARSET',           'utf8' );


/* MySQL database table prefix. */
$table_prefix =                 $_SERVER['DB_PRET'];


/* Authentication Unique Keys and Salts. https://api.wordpress.org/secret-key/1.1/salt/ */
define('AUTH_KEY',              'Ra=`oLU0_nl-G6z]^<hW-xw|X1>-bWyN7yE?RoB*-aKB]K|!+b,sK9KFaa&SV36T');
define('SECURE_AUTH_KEY',       'SG?V6G$0k|0u7tsUt|=O,Zpwqp(4^VwuqEZl``dn+B:x<h>DOn* rymW!k*$H:Oc');
define('LOGGED_IN_KEY',         '(@~@4:!t9t_*u,vp#Jch.z/DS3Ls|u7Pnv5YO{.PY?-dr0AX>|nF4|Hr/#.i!u5L');
define('NONCE_KEY',             '}Y7Bjo,00aP]J_op0V(`NoCP|:?A=oFxFa>e-Y#/hITUH #Vw6_DlL8=Z&oIL+^p');
define('AUTH_SALT',             '.qAj|~:l[P/{4V*|9/2,V+]n=S~n`Wde7Uk9-_vNZK3`{GyEf1H+LM]&z;7N+X(!');
define('SECURE_AUTH_SALT',      '1Aj1{)UN:|$%-+$]i,h#Y?roJA2Go(YepkY8[bGKJE.h~^-E^$~Pa7EY>D}Vj]E3');
define('LOGGED_IN_SALT',        'V^Qv.1{NBC:?}tXJT2N,Fmi*i20tUN2k5jAN&|hhe<iLQ$tq&nvFz,S}gFN+P%+e');
define('NONCE_SALT',            '8#|)lYbo3YBQb6$c+%.d&3{ETxJ)E?zNmn$S9BFv+|$-a7Zz9:|_-q+3wvm2#toc');


/* WordPress Localized Language. */
define( 'WPLANG',               '' );


/* Specify revisions, and maximum number of revisions. */
define( 'WP_POST_REVISIONS',    true );
define( 'WP_POST_REVISIONS',    '5' );


/* Media Trash. */
define( 'MEDIA_TRASH',          true );


/* Multisite. */
define( 'WP_ALLOW_MULTISITE',   false );


/* WordPress debug mode for developers. */
define( 'WP_DEBUG',             true ); # turn on or off
define( 'WP_DEBUG_LOG',         true ); # write to /wp-content/debug.log; review all notices including AJAX requests or wp-cron runs
define( 'WP_DEBUG_DISPLAY',     false ); # show inside the HTML of pages
define( 'SCRIPT_DEBUG',         false ); # force WP to use "dev" versions of CSS & JS files rather than the minified versions
define( 'SAVEQUERIES',          false ); # saves the database queries to an array and that array can be displayed to help analyze those queries


/* PHP Memory */
define( 'WP_MEMORY_LIMIT',      '128M' );
define( 'WP_MAX_MEMORY_LIMIT',  '256M' );


/* WordPress Cache */
define( 'WP_CACHE',             false ); # if true, includes the wp-content/advanced-cache.php script


/* Empty Trash */
define( 'EMPTY_TRASH_DAYS',     30 ); // 30 days


/* Cleanup Image Edits */
define( 'IMAGE_EDIT_OVERWRITE', true );


/* Disable the Plugin and Theme Editor */
define( 'DISALLOW_FILE_EDIT',   true );


/* Compression */
define( 'COMPRESS_CSS',         true );
define( 'COMPRESS_SCRIPTS',     true );
define( 'CONCATENATE_SCRIPTS',  true );
define( 'ENFORCE_GZIP',         true );


/* Administration Over SSL */
define( 'FORCE_SSL_LOGIN',      true );
define( 'FORCE_SSL_ADMIN',      true );


/* Override of default file permissions */
define( 'FS_CHMOD_DIR',         ( 0755 & ~ umask() ) );
define( 'FS_CHMOD_FILE',        ( 0644 & ~ umask() ) );


/* Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') ) define('ABSPATH', dirname(__FILE__) . '/');


/* Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

